fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => {
        return response.json()
    })
    .then((dataOfUser) => {
        return Promise.all(dataOfUser.map((user) => getAllTods(user)))
    })
    .then((response) => {
        response.forEach((data) => {
            displayToo(data)
        })
    })
    .catch((error) => {
        console.error(error.message);
    })
    
function getAllTods(user) {
    return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${user.id}`)
        .then((responseOfTodo) => {
            return responseOfTodo.json()
        })
        .then((dataOfTodo) => {
            const todoObj = {
                userName: user.name,
                allTodos: dataOfTodo.slice(0, 7)
            }
            return todoObj
        })
        .catch((error) => {
            console.error(error.message);
        })
}