const displayToo = (data)=>{
    const container=document.getElementById("container");
    const card=document.createElement("div")
    const h1=document.createElement("h1")
    const headText = document.createTextNode(data.userName)
    h1.appendChild(headText)
    card.appendChild(h1)
    const cardChild=document.createElement("div")
    const alltodos=data.allTodos
    alltodos.forEach(todo => {
        cardChild.innerHTML +=`        
        <p> <input type="checkbox" name="" id=""> ${todo.title}</p>
        `
        card.appendChild(cardChild)
    });
    container.appendChild(card)
}